//
//  ReturnViewController.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import UIKit
import CoreData

class ReturnViewController: UIViewController {
    
    var returnRecipientName: [Recipient] = []
    var returnLoanedTools: [Tools] = []
    var queryFriendName = ""
    
    @IBOutlet weak var returnTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fetchData()
    }
    
    func fetchData(){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest: NSFetchRequest<Recipient> = Recipient.fetchRequest()
        
        fetchRequest.predicate = NSPredicate(format: "recipientName = %@ AND recipientLoanQty > 0", queryFriendName)
        
        returnRecipientName = []
        
        do{
            returnRecipientName = try managedContext.fetch(fetchRequest)
            returnTableView.reloadData()
        }
        catch{
            print("Gagal memanggil")
        }
    }
    
    
    func saveData(toolName: String)
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let toolsFetchRequest: NSFetchRequest<Tools> = Tools.fetchRequest()
        //query
        toolsFetchRequest.predicate = NSPredicate(format: "toolName = %@", toolName)
        
        let recipientFetchRequest: NSFetchRequest<Recipient> = Recipient.fetchRequest()
        //query
        recipientFetchRequest.predicate = NSPredicate(format: "loanedItemName = %@ AND recipientName = %@", toolName, queryFriendName)
        
        returnLoanedTools = []
        returnRecipientName = []
        
        do{
            returnLoanedTools = try managedContext.fetch(toolsFetchRequest)
            returnRecipientName = try managedContext.fetch(recipientFetchRequest)
            returnRecipientName[0].recipientLoanQty -= 1
            returnLoanedTools[0].loanedQty -= 1
            returnLoanedTools[0].stockQty += 1
            
            try managedContext.save()
            returnTableView.reloadData()
        }
        catch{
            print("Gagal memanggil")
        }
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
}

extension ReturnViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return returnRecipientName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = returnTableView.dequeueReusableCell(withIdentifier: "returnCellsIdentifier") as! ReturnTableViewCell
        cell.returnToolImageView.image = UIImage(named: returnRecipientName[indexPath.row].loanedItemName!)
        cell.returnToolNameLabel.text = returnRecipientName[indexPath.row].loanedItemName
        cell.returnLoanedQty.text = "Loaned:  \(String(returnRecipientName[indexPath.row].recipientLoanQty))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        saveData(toolName: (tableView.cellForRow(at: indexPath) as! ReturnTableViewCell).returnToolNameLabel.text!)
    }
    
}

