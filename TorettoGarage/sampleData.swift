//
//  sampleData.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import Foundation

var sampleToolName = ["Wrench", "Cutter", "Pliers", "Screwdriver", "Welding Machine", "Welding Glasses", "Hammer", "Measuring Tape", "Alan Key Set", "Air Compressor"]

var sampleToolStock = [6, 15, 12, 13, 3, 7, 4, 9, 4, 2]

var sampleRecipientList = ["Brian", "Luke", "Letty", "Shaw", "Parker"]



