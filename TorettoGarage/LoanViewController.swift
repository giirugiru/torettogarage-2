//
//  LoanViewController.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import UIKit

class LoanViewController: UIViewController {
    
    @IBOutlet weak var loanTableView: UITableView!
    
    var seguedDataIndex = 0
    var recipientNameCont : [Recipient] = []
    var recipientLoanedToolsCont : [Tools] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        fetchData()
    }
    
    
    //Function untuk Fetch Data
    func fetchData(){
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        recipientNameCont = []
        recipientLoanedToolsCont = []
        
        do{
            recipientNameCont = try managedContext.fetch(Recipient.fetchRequest())
            recipientLoanedToolsCont = try managedContext.fetch(Tools.fetchRequest())
            loanTableView.reloadData()
        }
        catch{
            print("Gagal memanggil")
        }
    }
    
    
    
    // Save Data
    func saveData(selectedItem: Tools, rowIndex: Int, borrowerName: String)
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        selectedItem.stockQty -= 1
        selectedItem.loanedQty += 1
        
        for i in 0...recipientNameCont.count-1
        {
            if recipientNameCont[i].recipientName == borrowerName && recipientNameCont[i].loanedItemName == selectedItem.toolName
            {
                recipientNameCont[i].recipientLoanQty += 1
                break
            }
            
            if i == recipientNameCont.count-1
            {
                let newBorrowFriendEntry = Recipient(context: managedContext)
                newBorrowFriendEntry.recipientLoanQty = 1
                newBorrowFriendEntry.recipientName = borrowerName
                newBorrowFriendEntry.loanedItemName = selectedItem.toolName
                break;
            }
            
        }
        
        do {
            try managedContext.save()
            print("Berhasil menyimpan")
        } catch  {
            print("Gagal menyimpan")
        }
        
        navigationController?.popViewController(animated: true)
        
    }
}


// Template core data untuk tampilin di Table View Cell
extension LoanViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(recipientNameCont.count)
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "loanCellsIdentifier") as! LoanTableViewCell
        cell.loanRecipientImageView.image = UIImage(named: sampleRecipientList[indexPath.row])
        cell.loanRecipientNameLabel.text = sampleRecipientList[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let borrowerName = (tableView.cellForRow(at: indexPath) as! LoanTableViewCell).loanRecipientNameLabel
        if recipientLoanedToolsCont[self.seguedDataIndex].stockQty != 0
        {
            saveData(selectedItem: recipientLoanedToolsCont[self.seguedDataIndex], rowIndex: indexPath.row, borrowerName: (borrowerName?.text)!)
        }else{
            let alert = UIAlertController(title: "Stock is empty!", message: "The tool you want to loan is currently out of stock", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
}
