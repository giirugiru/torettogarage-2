//
//  LoanTableViewCell.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import UIKit

class LoanTableViewCell: UITableViewCell {
    
    @IBOutlet weak var loanRecipientImageView: UIImageView!
    
    @IBOutlet weak var loanRecipientNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
