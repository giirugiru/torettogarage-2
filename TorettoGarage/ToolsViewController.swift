//
//  ToolsViewController.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import UIKit

class ToolsViewController: UIViewController {
    
    @IBOutlet weak var toolTableView: UITableView!
    
    //Declare entity
    var toolNameCont : [Tools] = []
    var recipientNameCont : [Recipient] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Fetch Sample Data for first launch
        if UserDefaults.standard.bool(forKey: "FirstTimeLaunch") == false {
            appendData()
            fetchData()
            UserDefaults.standard.setValue(true, forKey: "FirstTimeLaunch")
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        fetchData()
    }
    
    
    
    //Function untuk Fetch Data
    func fetchData(){
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        toolNameCont = []
        
        do{
            toolNameCont = try managedContext.fetch(Tools.fetchRequest())
            toolTableView.reloadData()
        }
        catch{
            print("Gagal memanggil")
        }
    }
    
    
    
    //Function Append Sample Data
    func appendData()
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        for i in 1...sampleToolName.count
        {
            let newTool = Tools(context: managedContext)
            newTool.toolName = sampleToolName[i-1]
            newTool.stockQty = Int64(sampleToolStock[i-1])
            newTool.loanedQty = 0
            
            do {
                try managedContext.save()
            }
            catch{
                print("Gagal Menyimpan")
            }
        }
        
        
        for i in 1...sampleRecipientList.count
        {
            let newRecipient = Recipient(context: managedContext)
            newRecipient.recipientName = sampleRecipientList[i-1]
            newRecipient.recipientLoanQty = 0
            newRecipient.loanedItemName = ""
            
            //print("\(sampleRecipientList[i-1])")
            
            do {
                try managedContext.save()
            }
            catch{
                print("Gagal Menyimpan")
            }
        }
        
        
    }
    
    
}


// Template core data untuk tampilin di Table View Cell
extension ToolsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return toolNameCont.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "toolCellsIdentifier") as! ToolsTableViewCell
        cell.toolsImageView.image = UIImage(named: sampleToolName[indexPath.row])
        cell.toolNameLabel.text = toolNameCont[indexPath.row].toolName
        cell.toolStockLabel.text = "Stock: \(String(toolNameCont[indexPath.row].stockQty))"
        cell.toolLoanedLabel.text = "Loaned:  \(String(toolNameCont[indexPath.row].loanedQty))"
        
        return cell
    }
    
    //Biar cell nya gede
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemDataIndex = indexPath.row
        let itemStock = (tableView.cellForRow(at: indexPath) as! ToolsTableViewCell).toolStockLabel.text
        
        // Validasi ketika stock nya kosong, gak bisa di loan
        if itemStock == "Stock: 0" {
            let alert = UIAlertController(title: "Stock is empty!", message: "The tool you want to loan is currently out of stock", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }else{
        performSegue(withIdentifier: "segueToLoanViewController", sender: itemDataIndex)
        }
    }
    
    //Segue ke loanvc
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToLoanViewController"
        {
            let destination = segue.destination as! LoanViewController
            destination.seguedDataIndex = sender as! Int
        }
    }
    
    
    
}
