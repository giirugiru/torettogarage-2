//
//  ReturnTableViewCell.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import UIKit

class ReturnTableViewCell: UITableViewCell {

    @IBOutlet weak var returnToolImageView: UIImageView!
    
    @IBOutlet weak var returnToolNameLabel: UILabel!
    
    @IBOutlet weak var returnLoanedQty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
