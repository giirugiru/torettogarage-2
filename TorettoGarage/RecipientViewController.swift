//
//  RecipientViewController.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import UIKit

class RecipientViewController: UIViewController {
    
    
    @IBOutlet weak var recipientTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
}

// Template core data untuk tampilin di Table View Cell
extension RecipientViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(recipientNameCont.count)
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipientCellsIdentifier") as! RecipientTableViewCell
        cell.recipientImageView.image = UIImage(named: sampleRecipientList[indexPath.row])
        cell.recipientNameLabel.text = sampleRecipientList[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let queryName = (tableView.cellForRow(at: indexPath) as! RecipientTableViewCell).recipientNameLabel.text
        performSegue(withIdentifier: "segueToReturnViewController", sender: queryName)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToReturnViewController"
        {
            let destination = segue.destination as! ReturnViewController
            destination.queryFriendName = sender as! String
        }
    }

}
