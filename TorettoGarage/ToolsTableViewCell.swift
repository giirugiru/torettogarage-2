//
//  ToolsTableViewCell.swift
//  TorettoGarage
//
//  Created by Gilang Sinawang on 18/11/20.
//

import UIKit

class ToolsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var toolsImageView: UIImageView!
    
    @IBOutlet weak var toolNameLabel: UILabel!
    
    
    @IBOutlet weak var toolStockLabel: UILabel!
    
    @IBOutlet weak var toolLoanedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
